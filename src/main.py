from __future__ import annotations
import cadquery as cq
import math
import os
import pathlib
import sys
try:
    import drawSvg as draw
except:
    import drawsvg as draw

from typing import Optional

from terminal_c import (
    Vector,
    Line,
    VectorPath,
)


def get_output_base_dir():
    self_dir_str = os.path.dirname(__file__)
    out_dir_str = os.path.join(self_dir_str, "..", "build")
    out_path = pathlib.Path(out_dir_str).resolve()
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    return out_path


def scale(workplane: cq.Workplane, x: float, y: Optional[float] = None, z: Optional[float] = None) -> cq.Workplane:
    """
    Helper to scale a Workplane shape.
    Taken from https://github.com/CadQuery/cadquery/issues/638
    """
    y = y if y is not None else x
    z = z if z is not None else x
    t = cq.Matrix([
        [x, 0, 0, 0],
        [0, y, 0, 0],
        [0, 0, z, 0],
        [0, 0, 0, 1]
    ])
    return workplane.newObject([
        o.transformGeometry(t) if isinstance(o, cq.Shape) else o
        for o in workplane.objects
    ])


class Nut:

    def __init__(self, width: float, height: float, screw_diameter: float):
        self.width = width
        self.height = height
        self.diameter = self.width / 2 * 2 / math.sqrt(3)
        self.screw_diameter = screw_diameter

    def __call__(self) -> cq.Workplane:
        hexagon = (
            cq.Sketch()
            .regularPolygon(self.diameter, 6)
        )
        result = (
            cq.Workplane()
            .cylinder(60, self.screw_diameter / 2)
            .placeSketch(
                hexagon
            ).extrude(self.height)
        )
        return result

    @property
    def inlet_cutaway(self):
        nut = self()
        chamber_length = 30
        chamber = (
            cq.Workplane()
            .box(self.width, chamber_length, self.height)
            .translate(cq.Vector(0, -chamber_length / 2, self.height / 2))
        )
        return nut + chamber


class CushionConfig:
    def __init__(
            self,
            height: float = 16,
            straight_segment_height: float = 3,
            middle_layer_height: float = 12,
            middle_layer_scale_x: float = 0.96,
            middle_layer_scale_y: float = 0.96,
            middle_layer_fillet_scale: float = 0.8,
            top_layer_scale_x: float = 0.89,
            top_layer_scale_y: float = 0.90,
            top_layer_fillet_scale: float = 0.4,
            upper_cutout_indent: float = 2,
            upper_cutout_radius: float = 5,
            upper_cutout_width_factor: float = 0.9,
            lower_cutout_indent: float = 2 + 2,
            lower_cutout_radius: float = 5,
            lower_cutout_width_factor: float = 0.28,
            lower_cutout_width_factor_front: float = 0.5,
            lower_cutout_width_factor_before_front: float = 0.42,
            lower_cutout_relative_third_position: float = 0.2,
    ):
        self.height = height
        self.straight_segment_height = straight_segment_height
        self.middle_layer_height = middle_layer_height
        self.middle_layer_scale_x = middle_layer_scale_x
        self.middle_layer_scale_y = middle_layer_scale_y
        self.middle_layer_fillet_scale = middle_layer_fillet_scale
        self.top_layer_scale_x = top_layer_scale_x
        self.top_layer_scale_y = top_layer_scale_y
        self.top_layer_fillet_scale = top_layer_fillet_scale
        self.upper_cutout_indent = upper_cutout_indent
        self.upper_cutout_radius = upper_cutout_radius
        self.upper_cutout_width_factor = upper_cutout_width_factor
        self.lower_cutout_indent = lower_cutout_indent
        self.lower_cutout_radius = lower_cutout_radius
        self.lower_cutout_width_factor = lower_cutout_width_factor
        self.lower_cutout_width_factor_front = lower_cutout_width_factor_front
        self.lower_cutout_relative_third_position = lower_cutout_relative_third_position
        self.lower_cutout_width_factor_before_front = lower_cutout_width_factor_before_front


class FoamFillConfig:
    def __init__(
        self,
        distance_to_middle_basin: tuple[float, float] = (17, 2),
        foam_over_bar_height: float = 9,
    ):
        self.distance_to_middle_basin = distance_to_middle_basin
        self.foam_over_bar_height = foam_over_bar_height


class AnchorConfig:
    def __init__(
        self,
        base_height: float = 5,
        bar_width: float = 8,
        bar_length: float = 76,
        bar_height: float = 5.2,
        inner_cutout_plate_fillet: float = 0.1,
        bottom_chamfer: float = 0.9,
        stitch_hole_bottom_chamfer: float = 0.2,
        stitch_distance: float = 5.0,
        stitch_hole_diameter: float = 2.5,
    ):
        self.base_height = base_height
        self.bar_width = bar_width
        self.bar_length = bar_length
        self.bar_height = bar_height
        self.inner_cutout_plate_fillet = inner_cutout_plate_fillet
        self.bottom_chamfer = bottom_chamfer
        self.stitch_hole_bottom_chamfer = stitch_hole_bottom_chamfer
        self.stitch_distance = stitch_distance
        self.stitch_hole_diameter = stitch_hole_diameter


class MoldConfig:
    def __init__(
            self,
            # diameter of the holes for filling the silicone
            channel_diameter: float = 14,
            # diameter for the screw _head_ holes
            screw_head_diameter: float = 7,
            # how hight the part of the screw hole on the upper part is before the screw head begins
            screw_head_elevation: float = 20 - 4.8 - 2 - 3,  # length - base - nut - ???
            # how much the silicone channels are moved out of the upper-part mold
            channel_extra_indent: float = 3,
            # how much the silicone channel are moved from the maximum position towards the middle
            channel_offset: float = -5,
    ):
        self.channel_diameter = channel_diameter
        self.screw_head_diameter = screw_head_diameter
        self.screw_head_elevation = screw_head_elevation
        self.channel_extra_indent = channel_extra_indent
        self.channel_offset = channel_offset


class BaseConfig:
    def __init__(
        self,
        # diameter of the scew-holes (it may be necessary to use a different clearance for the base)
        screw_diameter: float = 3.6,
        # diameter for the screw _head_ holes (it may be necessary to use a different clearance for the base)
        screw_head_diameter: float = 6,
        bottom_height: float = 1.6,
        wall_height: float = 3.6,
        side_clearance: float = 1.4,
        wall_thickness: float = 2,
        top_fillet: float = 0.9,
        screw_indent: float = 0.4,
        elevation_delta_x: float = 7,
        elevation_delta_y: float = 9,
        elevation_height: float = 1.8,
        cutout_delta_x: float = 14,
        cutout_delta_y: float = 10.5,
        cutout_depth: float = 1.0,
    ):
        self.screw_diameter = screw_diameter
        self.screw_head_diameter = screw_head_diameter
        self.bottom_height = bottom_height
        self.wall_height = wall_height
        self.side_clearance = side_clearance
        self.wall_thickness = wall_thickness
        self.top_fillet = top_fillet
        self.screw_indent = screw_indent
        self.elevation_delta_x = elevation_delta_x
        self.elevation_delta_y = elevation_delta_y
        self.elevation_height = elevation_height
        self.cutout_delta_x = cutout_delta_x
        self.cutout_delta_y = cutout_delta_y
        self.cutout_depth = cutout_depth


class Rest:
    def __init__(
        self,
        cushion_config: CushionConfig,
        anchor_config: AnchorConfig,
        foam_fill_config: FoamFillConfig,
        mold_config: MoldConfig,
        base_config: BaseConfig,
        width_front: float = 110,
        width_back: float = 125,
        length: float = 100,
        horizontal_fillet: float = 5,
        vertical_fillet: float = 15,
        seam_indent: float = 8.5,
        seam_width: float = 4.5,
        leather_squeeze_reduction: float = 3,
        screw_diameter: float = 4.2,
    ):
        self.cushion_c = cushion_config
        self.anchor_c = anchor_config
        self.foam_c = foam_fill_config
        self.mold_c = mold_config
        self.base_c = base_config
        self.width_front = width_front
        self.width_back = width_back
        self.length = length
        self.horizontal_fillet = horizontal_fillet
        self.vertical_fillet = vertical_fillet
        self.cusion_mold_box_oversize = 6
        self.nut = Nut(
            width=5.5 + 0.2,
            height=2.4 + 0.5,
            screw_diameter=screw_diameter
        )
        self.anchor_bar_x_distance = (self.width_back + self.width_front) / 4 - 8 - self.anchor_c.bar_width / 2
        self.cushion_vertical_clearance = 0.4
        self.cushion_horizontal_clearance = 0.2
        self.seam_indent = seam_indent
        self.seam_width = seam_width
        self.leather_squeeze_reduction = leather_squeeze_reduction
        self.screw_diameter = screw_diameter

    def get_base_points(
            self,
            x_factor: float = 1,
            y_factor: float = 1,
            x_reduction: float = 0,
            y_reduction: float = 0,
    ) -> list[tuple[float, float]]:
        x_front = x_factor * self.width_front / 2 - x_reduction
        x_back = x_factor * self.width_back / 2 - x_reduction
        y = y_factor * self.length / 2 - y_reduction
        return [
            (-x_front, -y),
            (-x_back, y),
            (x_back, y),
            (x_front, -y)
        ]

    def get_base_seam_cutout(
            self,
            ix: int,
            x_factor: float = 1,
            y_factor: float = 1,
            x_reduction: float = 0,
            y_reduction: float = 0,
            fillet: Optional[float] = None,
    ) -> cq.Sketch:
        assert ix in range(4)
        fillet = fillet if fillet is not None else self.vertical_fillet
        fillet_indent = math.sqrt(((math.sqrt(2 * fillet**2) - fillet) ** 2) / 2)
        seam_indent_inner_points = self.get_base_points(
            x_factor,
            y_factor,
            x_reduction + fillet_indent,
            y_reduction + fillet_indent,
        )
        if ix in [0, 2]:
            angle = 45 + 90
        else:
            angle = 45 + 180
        return (
            cq.Sketch()
            .push([seam_indent_inner_points[ix]])
            .rect(self.seam_width, self.seam_indent, angle=angle)
            .reset()
            .vertices()
            .fillet(self.seam_width / 3)
        )

    def get_base_form(
            self,
            x_factor: float = 1,
            y_factor: float = 1,
            x_reduction: float = 0,
            y_reduction: float = 0,
            fillet: Optional[float] = None,
    ) -> cq.Sketch:
        points = self.get_base_points(x_factor, y_factor, x_reduction, y_reduction)
        fillet = fillet if fillet is not None else self.vertical_fillet
        return (
            cq.Sketch()
            .segment(points[0], points[1])
            .segment(points[2])
            .segment(points[3])
            .close()
            .assemble()
            .vertices()
            .fillet(fillet)
        )

    @property
    def cushion_mold_base_form(self) -> cq.Sketch:
        return self.get_base_form(
            x_factor=1,
            y_factor=1,
            x_reduction=-self.cusion_mold_box_oversize,
            y_reduction=-self.cusion_mold_box_oversize,
            fillet=3
        )

    @property
    def cushion_mold_screw_position(self) -> list[tuple[float, float]]:
        d = self.cusion_mold_box_oversize - 5
        x1 = self.width_front / 2 + d
        x2 = self.width_back / 2 + d
        y = self.length / 2 + d
        positions = [
            (x1, -y),
            (-x1, -y),
            (x2, y),
            (-x2, y),
        ]
        return positions

    @property
    def cushion_mold_screw_holes(self) -> cq.Workplane:
        positions = self.cushion_mold_screw_position
        result = cq.Workplane()
        for p in positions:
            result += (
                cq.Workplane()
                .cylinder(height=100, radius=self.screw_diameter / 2)
                .translate(cq.Vector(p[0], p[1], 0))
            )
        return result

    @property
    def cushion_mold_lower_part(self) -> cq.Workplane:
        return (
                self._factor_cushion_anchor_base_plate(is_for_mold=True)
                +
                self._factor_anchor_bars(is_for_mold=True, is_for_cushion_tool=False)
                -
                self.cushion_mold_screw_holes
        )

    @property
    def cushion_mold_upper_part(self) -> cq.Workplane:
        # diameter of the holes for filling the silicone
        channel_diameter = self.mold_c.channel_diameter
        # diameter for the screw _head_ holes
        screw_head_diameter = self.mold_c.screw_head_diameter
        # how hight the part of the screw hole on the upper part is before the screw head begins
        screw_head_elevation = self.mold_c.screw_head_elevation
        # how much the silicone channels are moved out of the upper-part mold
        channel_extra_indent = self.mold_c.channel_extra_indent
        # how much the silicone channel are moved from the maximum position towards the middle
        channel_offset = (
            self.width_front / 2
            -
            self.vertical_fillet
            -
            channel_diameter / 2
            -
            self.mold_c.channel_offset
        )
        block = (
            cq.Workplane()
            .placeSketch(
                self.cushion_mold_base_form
            )
            .extrude(self.cushion_c.height + 3)
        )
        channel_point_1 = (-channel_offset, -self.length / 2, 0)
        channel_point_2 = (channel_offset, -self.length / 2, 0)
        channels = (
            cq.Workplane()
            .pushPoints([
                channel_point_1,
                channel_point_2
            ])
            .cylinder(100, channel_diameter / 2)
            .rotate(channel_point_1, channel_point_2, 90)
        ).translate(cq.Vector(0, 0, channel_extra_indent))
        screw_cones = cq.Workplane()
        for p in self.cushion_mold_screw_position:
            screw_cones = (
                    screw_cones
                    +
                    cq.Solid.makeCone(0, screw_head_diameter / 2, screw_head_diameter / 2)
                    .translate(p)
                    .translate(cq.Vector(0, 0, -100 / 2 + screw_head_elevation))
            )
        screw_head_holes = (
                (
                    cq.Workplane()
                    .pushPoints(self.cushion_mold_screw_position)
                    .cylinder(100, screw_head_diameter / 2)
                    .translate(cq.Vector(0, 0, screw_head_elevation + screw_head_diameter / 2))
                )
                +
                screw_cones
        ).translate(cq.Vector(0, 0, 50))
        return (
                block
                -
                self.cushion_mold_screw_holes
                -
                self.cushion
                -
                channels
                -
                screw_head_holes
        ).translate(cq.Vector(0, 0, self.anchor_c.base_height))

    def _factor_cushion_top_cutout(self, for_squeeze: bool) -> cq.Workplane:
        clearance = self.leather_squeeze_reduction if for_squeeze else 0
        return (
                self._factor_cushion_cutout(
                    indent=self.cushion_c.upper_cutout_indent,
                    radius=self.cushion_c.upper_cutout_radius,
                    width_factor=self.cushion_c.upper_cutout_width_factor,
                    width_reduction=clearance,
                )
                +
                self._factor_cushion_cutout(
                    indent=self.cushion_c.lower_cutout_indent,
                    radius=self.cushion_c.lower_cutout_radius,
                    width_factor=self.cushion_c.lower_cutout_width_factor,
                    width_reduction=clearance,
                    width_factor_front=self.cushion_c.lower_cutout_width_factor_front,
                    width_factor_before_front=self.cushion_c.lower_cutout_width_factor_before_front
                )
        )

    @property
    def leather_squeeze(self):
        base_height = 3
        plate_base_level = self.cushion_c.height
        cutout = self._factor_cushion_top_cutout(for_squeeze=True)
        plate_sketch = self.get_base_form(
            self.cushion_c.middle_layer_scale_x,
            self.cushion_c.middle_layer_scale_y,
            fillet=self.vertical_fillet * self.cushion_c.top_layer_fillet_scale,
        )
        plate = (
            cq.Workplane()
            .placeSketch(plate_sketch)
            .extrude(base_height)
            .translate(cq.Vector(0, 0, plate_base_level + 1 * base_height))
        )
        cut_box = (
            cq.Workplane()
            .box(self.width_front + 10, self.length + 10, 50)
            .translate(cq.Vector(0, 0, plate_base_level + base_height + 25))
        )
        result = (
                cutout.intersect(
                    cq.Workplane()
                    .placeSketch(plate_sketch)
                    .extrude(100)
                )
                -
                cut_box
                +
                plate
        )
        return result

    def _factor_cushion_cutout(
            self,
            indent: float,
            radius: float,
            width_factor: float,
            width_reduction: float,
            width_factor_front: Optional[float] = None,
            width_factor_before_front: Optional[float] = None,
    ) -> cq.Workplane:
        """
        Factors a cylindric cut-out which is used for both, the bigger upper and the smaller lower cut-out.

        :param indent: how deep indented into the cushion
        :param radius: the radius in z-direction which goes into the cushion; the horizontal one is calculated from
                       the cushion width
        :param width_factor: factor how big the horizontal radius is compared to the cusion
        """
        base_points = self.get_base_points(1, 1)
        back_width = base_points[2][0]
        front_width = base_points[3][0]
        if width_factor_front is None:
            width_factor_front = width_factor
        radius_upper = back_width * width_factor - width_reduction
        radius_lower = front_width * width_factor_front - width_reduction
        length = base_points[1][1] - base_points[0][1]
        sketches = [
            cq.Sketch().ellipse(radius_lower, radius),
            cq.Sketch().ellipse(radius_upper, radius).moved(cq.Location(cq.Vector(0, 0, length)))
        ]
        if width_factor_before_front is not None:
            relative_third_position = self.cushion_c.lower_cutout_relative_third_position
            x_at_position = front_width + relative_third_position * (back_width - front_width)
            radius_middle = x_at_position * width_factor_before_front - width_reduction
            sketches.insert(
                1,
                cq.Sketch().ellipse(radius_middle, radius).moved(
                    cq.Location(cq.Vector(0, 0, relative_third_position * length))
                )
            )
        return (
            cq.Workplane()
            .placeSketch(
                *sketches
            )
            .loft()
            .rotate(axisStartPoint=(-1, 0), axisEndPoint=(1, 0), angleDegrees=-90)
            .translate(cq.Vector(
                0,
                -(length / 2),
                self.cushion_c.height + radius - indent
            ))
        )

    @property
    def pattern(self):
        base_form = self.get_base_form(
            self.cushion_c.top_layer_scale_x,
            self.cushion_c.top_layer_scale_y,
            self.vertical_fillet * self.cushion_c.top_layer_fillet_scale
        )
        return base_form

    @property
    def cushion(self) -> cq.Workplane:
        def _get_lofted_cushion_element(func, *args) -> cq.Workplane:
            return (
                cq.Workplane()
                .placeSketch(
                    func(*args),
                    func(*args).moved(cq.Location(cq.Vector(0, 0, self.cushion_c.straight_segment_height))),
                    func(
                        *args,
                        self.cushion_c.middle_layer_scale_x,
                        self.cushion_c.middle_layer_scale_y,
                        fillet=self.vertical_fillet * self.cushion_c.middle_layer_fillet_scale
                    ).moved(cq.Location(cq.Vector(0, 0, self.cushion_c.middle_layer_height))),
                    func(
                        *args,
                        self.cushion_c.top_layer_scale_x,
                        self.cushion_c.top_layer_scale_y,
                        fillet=self.vertical_fillet * self.cushion_c.top_layer_fillet_scale,
                    ).moved(cq.Location(cq.Vector(0, 0, self.cushion_c.height))),
                )
                .loft()
            )

        base_form = (
            _get_lofted_cushion_element(self.get_base_form)
            -
            self._factor_cushion_top_cutout(for_squeeze=False)
        )
        base_form = base_form.edges("not <Z").fillet(self.horizontal_fillet)
        sc1 = _get_lofted_cushion_element(self.get_base_seam_cutout, 0)
        sc2 = _get_lofted_cushion_element(self.get_base_seam_cutout, 1)
        sc3 = _get_lofted_cushion_element(self.get_base_seam_cutout, 2)
        sc4 = _get_lofted_cushion_element(self.get_base_seam_cutout, 3)
        return base_form - sc1 - sc2 - sc3 - sc4

    def factor_screw_inlet(self) -> cq.Workplane:
        return self.nut.inlet_cutaway

    @property
    def side_angle(self) -> float:
        if self.width_back == self.width_front:
            return 0.0
        dx = math.fabs(self.width_back - self.width_front) / 2
        result = math.degrees(math.tan(dx / self.length))
        if self.width_front > self.width_back:
            result *= -1
        return result

    def _factor_cushion_anchor_base_plate(
            self,
            is_for_mold: bool
    ) -> cq.Workplane:
        if is_for_mold:
            base_sketch = self.cushion_mold_base_form
        else:
            base_sketch = self.get_base_form()
        base_height = self.anchor_c.base_height
        distance_to_middle_cutout = self.foam_c.distance_to_middle_basin
        base = (
            cq.Workplane()
            .placeSketch(
                base_sketch
            )
            .extrude(base_height)
        )
        if not is_for_mold:
            cutout = (
                cq.Workplane()
                +
                cq.Workplane().placeSketch(self.get_base_seam_cutout(0)).extrude(base_height)
                +
                cq.Workplane().placeSketch(self.get_base_seam_cutout(1)).extrude(base_height)
                +
                cq.Workplane().placeSketch(self.get_base_seam_cutout(2)).extrude(base_height)
                +
                cq.Workplane().placeSketch(self.get_base_seam_cutout(3)).extrude(base_height)
            )
            return base - cutout
        else:
            foam_block = (
                scale(self.cushion, 0.98)
                .translate(cq.Vector(0, 0, base_height - 1.8))
                .intersect(
                    cq.Workplane()
                    .placeSketch(
                        self.get_base_form(
                            x_reduction=distance_to_middle_cutout[0],
                            y_reduction=distance_to_middle_cutout[1],
                            fillet=0.5
                        )
                    )
                    .extrude(10)
                    .translate(cq.Vector(0, 0, base_height))
                )
            )
            return base + foam_block

    def _factor_anchor_bars(self, is_for_mold: bool, is_for_cushion_tool: bool) -> cq.Workplane:
        assert not (is_for_cushion_tool and is_for_mold)
        base_height = self.anchor_c.base_height
        bar_length = self.anchor_c.bar_length
        bar_width = self.anchor_c.bar_width
        bar_height = self.anchor_c.bar_height
        bar_x_distance = self.anchor_bar_x_distance

        if is_for_mold:
            bar_height += self.foam_c.foam_over_bar_height

        if is_for_cushion_tool:
            bar_height += (self.foam_c.foam_over_bar_height / 2)

        fillet = 1 if (is_for_mold or is_for_cushion_tool) else 1.2

        # add clearance
        h_clearance = (
            self.cushion_horizontal_clearance
            if is_for_mold
            else
            (
                0.6 if is_for_cushion_tool else 0
            )
        )

        def _local_bar_left():
            return (
                (
                    cq.Workplane()
                    .box(bar_width + 2 * h_clearance, bar_length + 2 * h_clearance, bar_height)
                    .translate(
                        cq.Vector(0.25 * bar_width, 0, base_height + 0.5 * bar_height))
                    .rotate((0, 0, 0), (0, 0, 1), -self.side_angle)
                )
                .translate(cq.Vector(bar_x_distance, 0, 0))
                .edges("not <Z")
                .fillet(fillet)
            )

        bar_left = _local_bar_left()
        bar_right = _local_bar_left().mirror(mirrorPlane="ZY", basePointVector=(0, 0, 0))
        return bar_right + bar_left

    @property
    def cusion_anchor(self) -> cq.Workplane:
        return (
            (
                self._factor_cushion_anchor_base_plate(is_for_mold=False)
                +
                self._factor_anchor_bars(is_for_mold=False, is_for_cushion_tool=False)
                -
                self.screw_bolts_with_nuts_cutouts
                -
                self.stitch_holes
            )
            .edges("%Line and <Z")
            .chamfer(self.anchor_c.bottom_chamfer)
            .edges("%Ellipse and <Z")
            .chamfer(self.anchor_c.stitch_hole_bottom_chamfer)
        )

    @property
    def screw_positions_and_angles(self) -> list[tuple[float, float, float]]:
        screw_x_distance = 16
        screw_y_offset = 0.25 * self.anchor_c.bar_width
        b = (self.anchor_c.bar_length - screw_x_distance) / 2
        alpha = self.side_angle
        dx = math.sin(math.radians(alpha)) * b
        dy = math.cos(math.radians(alpha)) * b
        result = [
            (self.anchor_bar_x_distance + dx + screw_y_offset, dy, 90 - alpha),
            (self.anchor_bar_x_distance - dx + screw_y_offset, -dy, 90 - alpha),
            (-self.anchor_bar_x_distance - dx - screw_y_offset, dy, -90 + alpha),
            (-self.anchor_bar_x_distance + dx - screw_y_offset, -dy, -90 + alpha),
        ]
        return result

    @property
    def screw_bolts_with_nuts_cutouts(self) -> cq.Workplane:
        nut_elevation = 0.3
        positions = self.screw_positions_and_angles
        result = cq.Workplane()
        for p in positions:
            result += (
                self.nut.inlet_cutaway
                .rotate((0, 0, 0), (0, 0, 1), p[2])
                .translate(cq.Vector(p[0], p[1], self.anchor_c.base_height + nut_elevation))
            )
        return result

    def factor_stitch_hole(self) -> cq.Workplane:
        diameter = self.anchor_c.stitch_hole_diameter
        return (
            cq.Workplane()
            .cylinder(20, diameter / 2)
            .rotate((0, 0, 0), (1, 0, 0), 45)
            .translate(cq.Vector(0, self.anchor_c.base_height - diameter, 1))
        )

    def _factor_stitch_line(self, p_start: Vector, p_end: Vector, angle) -> list[tuple[Vector, float]]:
        full_vector = p_end - p_start
        fillet_vector = full_vector.unit_vector * (self.vertical_fillet - 0 * self.anchor_c.stitch_hole_diameter / 2)
        stitch_line_start = p_start + fillet_vector
        stitch_line_end = p_end - fillet_vector
        stitch_vector = stitch_line_start - stitch_line_end
        length = stitch_vector.length
        d = self.anchor_c.stitch_distance
        num = math.floor(length / d)
        rest = length % d
        offset_vector = stitch_vector.unit_vector * (rest / 2)
        hole_d_vector = stitch_vector.unit_vector * -d
        positions = []
        for n in range(num + 1):
            positions.append(
                (stitch_line_start + offset_vector + hole_d_vector * n, angle)
            )
        return positions

    @property
    def stitch_holes(self) -> cq.Workplane:
        positions = (
                self._factor_stitch_line(
                    Vector(-self.width_front / 2, -self.length / 2),
                    Vector(+self.width_front / 2, -self.length / 2),
                    0
                )
                +
                self._factor_stitch_line(
                    Vector(-self.width_front / 2, -self.length / 2),
                    Vector(-self.width_back / 2, self.length / 2),
                    -90 + self.side_angle
                )
                +
                self._factor_stitch_line(
                    Vector(-self.width_back / 2, self.length / 2),
                    Vector(+self.width_back / 2, self.length / 2),
                    180
                )
                +
                self._factor_stitch_line(
                    Vector(self.width_front / 2, -self.length / 2),
                    Vector(self.width_back / 2, self.length / 2),
                    90 - self.side_angle
                )
        )

        result = cq.Workplane()

        for p in positions:
            result += (
                self.factor_stitch_hole()
                .rotate((0, 0, 0), (0, 0, 1), p[1])
                .translate(cq.Vector(p[0].x, p[0].y))
            )

        return result

    @property
    def cushion_tool(self) -> cq.Workplane:
        cushion = self.cushion
        bars = (
            self._factor_anchor_bars(is_for_mold=False, is_for_cushion_tool=True)
            .translate(cq.Vector(0, 0, -self.anchor_c.base_height))
        )
        return cushion - bars

    @property
    def screw_bolts_with_screw_heads(self) -> cq.Workplane:
        positions = self.screw_positions_and_angles
        result = cq.Workplane()
        screw_head_diameter = self.base_c.screw_head_diameter
        screw_diameter = self.base_c.screw_diameter

        for p in positions:
            result += (
                (
                    cq.Workplane()
                    .cylinder(60, screw_diameter / 2)
                    +
                    cq.Solid.makeCone(screw_head_diameter / 2, 0, screw_head_diameter / 2)
                    +
                    cq.Workplane()
                    .cylinder(60, screw_head_diameter / 2)
                    .translate(cq.Vector(0, 0, -30))
                )
                .translate(cq.Vector(p[0], p[1], 0))
            )
        return result

    @property
    def base(self) -> cq.Workplane:
        bottom_height = self.base_c.bottom_height
        wall_height = self.base_c.wall_height
        side_clearance = self.base_c.side_clearance
        wall_thickness = self.base_c.wall_thickness
        top_fillet = self.base_c.top_fillet
        screw_indent = self.base_c.screw_indent
        elevation_delta_x = self.base_c.elevation_delta_x
        elevation_delta_y = self.base_c.elevation_delta_y
        elevation_height = self.base_c.elevation_height
        cutout_delta_x = self.base_c.cutout_delta_x
        cutout_delta_y = self.base_c.cutout_delta_y
        cutout_depth = self.base_c.cutout_depth

        inner_base = self.get_base_form(
            x_reduction=-side_clearance,
            y_reduction=-side_clearance,
            fillet=self.vertical_fillet + side_clearance
        )
        outer_base = self.get_base_form(
            x_reduction=-side_clearance - wall_thickness,
            y_reduction=-side_clearance - wall_thickness,
            fillet=self.vertical_fillet + side_clearance + wall_thickness
        )
        elevation = self.get_base_form(
            x_reduction=elevation_delta_x,
            y_reduction=elevation_delta_y,
            fillet=self.vertical_fillet / 2
        )
        cutout = self.get_base_form(
            x_reduction=cutout_delta_x,
            y_reduction=cutout_delta_y,
            fillet=self.vertical_fillet / 2
        )

        return (
            (
                cq.Workplane()
                .placeSketch(
                    outer_base
                )
                .extrude(bottom_height + wall_height)
                -
                cq.Workplane()
                .placeSketch(
                    inner_base
                )
                .extrude(20)
                .translate(cq.Vector(0, 0, bottom_height))
            )
            .edges(">Z").fillet(top_fillet)
            .placeSketch(
                elevation
            ).extrude(elevation_height)
            -
            (
                cq.Workplane()
                .placeSketch(cutout)
                .extrude(20)
                .translate(cq.Vector(0, 0, -20 + cutout_depth))
            )
            -
            self.screw_bolts_with_screw_heads.translate(cq.Vector(0, 0, screw_indent))
        )

    @property
    def old_base(self) -> cq.Workplane:
        bottom_height = 2
        wall_height = 2
        side_clearance = 1.5
        wall_thickness = 2
        top_fillet = 0.9
        feet_diameter = 13
        feet_height = 2.5
        feet_side_front_d = 18
        feet_side_back_d = 20
        feet_face_d = 14
        screw_indent = 0.4

        dxf = self.width_front / 2 - feet_side_front_d
        dxb = self.width_back / 2 - feet_side_back_d
        dy = self.length / 2 - feet_face_d

        feet_positions = [
            (dxf, -dy),
            (-dxf, -dy),
            (dxb, dy),
            (-dxb, dy),
        ]

        feet = cq.Workplane()

        for feet_p in feet_positions:
            feet += (
                cq.Workplane()
                .cylinder(radius=feet_diameter / 2, height=2 * feet_height)
                .translate(cq.Vector(feet_p[0], feet_p[1], 0))
            )

        inner_base = self.get_base_form(
            x_reduction=-side_clearance,
            y_reduction=-side_clearance,
            fillet=self.vertical_fillet + side_clearance
        )
        outer_base = self.get_base_form(
            x_reduction=-side_clearance - wall_thickness,
            y_reduction=-side_clearance - wall_thickness,
            fillet=self.vertical_fillet + side_clearance + wall_thickness
        )
        return (
            (
                cq.Workplane()
                .placeSketch(
                    outer_base
                )
                .extrude(bottom_height + wall_height)
                -
                cq.Workplane()
                .placeSketch(
                    inner_base
                )
                .extrude(20)
                .translate(cq.Vector(0, 0, bottom_height))
            )
            .edges(">Z").fillet(top_fillet)
            -
            self.screw_bolts_with_screw_heads.translate(cq.Vector(0, 0, screw_indent))
            -
            feet
        )


class LeatherPattern:

    class Corner:
        def __init__(
                self,
                # base position
                offset: Vector,
                # angle of the corner (towards outside; upper left is 45°; increasing counter-clock)
                angle_direction: float,
                # how longer the bottom layer is on one side compared to the top layer; for the side on the right of the corner...
                bottom_overlength_right: float,
                # ... and for the left side
                bottom_overlength_left: float,
                # height of leather wall; for the side on the right of the corner...
                side_length_right: float,
                # ... and for the left
                side_length_left: float,
                # the width of the leather lips in the corner (the lip that folds into the stich-channels)
                lip_length: float,
                # the distance of stitch holes of the corner seams
                corner_seam_stitch_distance: float,
        ):
            self.offset = offset
            self.angle_direction = angle_direction
            self.bottom_overlength_right = bottom_overlength_right
            self.bottom_overlength_left = bottom_overlength_left
            self.side_length_right = side_length_right
            self.side_length_left = side_length_left
            self.lip_length = lip_length
            self.corner_seam_stitch_distance = corner_seam_stitch_distance

            self.helper_line_diagonal = Line(
                self.offset,
                self.offset + Vector(0, 30).rotate(self.angle_direction)
            )

            self.seam_distance = 0.5 * lip_length

            self.left_leg_v = Vector(
                -side_length_left,
                bottom_overlength_left
            ).rotate(-45).rotate(self.angle_direction)

            self.right_leg_v = Vector(
                side_length_right,
                bottom_overlength_right
            ).rotate(45).rotate(self.angle_direction)

            self.left_cut_p = (
                (
                    self.left_leg_v
                    +
                    self.left_leg_v.rotate(-90).unit_vector * self.lip_length
                )
                +
                self.offset
            )

            self.right_cut_p = (
                (
                    self.right_leg_v
                    +
                    self.right_leg_v.rotate(90).unit_vector * self.lip_length
                )
                +
                self.offset
            )

            self.left_seam_p = (
                (
                    self.left_leg_v
                    +
                    self.left_leg_v.rotate(-90).unit_vector * self.seam_distance
                )
                +
                self.offset
            )

            self.right_seam_p = (
                (
                    self.right_leg_v
                    +
                    self.right_leg_v.rotate(90).unit_vector * self.seam_distance
                )
                +
                self.offset
            )

            self.left_leg = (
                self.left_leg_v
                +
                self.offset
            )

            self.right_leg = (
                self.right_leg_v
                +
                self.offset
            )

            self.left_cut_i = self.helper_line_diagonal.intersection_with(
                Line(
                    self.left_cut_p,
                    self.left_cut_p + self.left_leg_v
                )
            )

            self.right_cut_i = self.helper_line_diagonal.intersection_with(
                Line(
                    self.right_cut_p,
                    self.right_cut_p + self.right_leg_v
                )
            )

            self.cut_i = (self.left_cut_i + self.right_cut_i) / 2

            self.left_seam_i = self.helper_line_diagonal.intersection_with(
                Line(
                    self.left_seam_p,
                    self.left_seam_p + self.left_leg_v
                )
            )

            self.right_seam_i = self.helper_line_diagonal.intersection_with(
                Line(
                    self.right_seam_p,
                    self.right_seam_p + self.right_leg_v
                )
            )

            self.seam_i = (self.left_seam_i + self.right_seam_i) / 2

            self.left_seam_line = Line(self.left_seam_p, self.seam_i)
            self.right_seam_line = Line(self.right_seam_p, self.seam_i)

            self.stitch_points = self._calc_stitch_points()

        def _calc_stitch_points(self) -> list[Vector]:
            start_offset = Line(self.seam_i, self.cut_i).length
            s = self.seam_i
            length = min(self.left_seam_line.length, self.right_seam_line.length) - start_offset
            num_x = math.floor(length / self.corner_seam_stitch_distance) + 1
            unit_v_left = self.left_seam_line.vector.unit_vector * -1
            unit_v_right = self.right_seam_line.vector.unit_vector * -1
            delta_v_left = unit_v_left * self.corner_seam_stitch_distance
            delta_v_right = unit_v_right * self.corner_seam_stitch_distance

            #if length % self.corner_seam_stitch_distance < 1.2:
            #    num_x -= 1

            offset_v_left = unit_v_left * start_offset
            offset_v_right = unit_v_right * start_offset

            result = []
            for i in range(num_x):
                result.append(s + offset_v_left + delta_v_left * i)
                result.append(s + offset_v_right + delta_v_right * i)
            return result

        @property
        def debug_lines(self) -> draw.Group:
            g = draw.Group()
            g.append(
                draw.Lines(*self.helper_line_diagonal.as_svg_coordinate_list),
            )
            g.append(
                draw.Circle(*self.helper_line_diagonal.b.as_tuple, 1)
            )
            g.append(
                draw.Circle(*self.left_leg.as_tuple, 1, fill='#00aa00')
            )
            g.append(
                draw.Circle(*self.right_leg.as_tuple, 1, fill='#22ff22')
            )
            g.append(
                draw.Circle(*self.left_cut_p.as_tuple, 1, fill='#aa0000')
            )
            g.append(
                draw.Circle(*self.right_cut_p.as_tuple, 1, fill='#ff2222')
            )
            g.append(
                draw.Circle(*self.left_seam_p.as_tuple, 1, fill='#0000aa')
            )
            g.append(
                draw.Circle(*self.right_seam_p.as_tuple, 1, fill='#2222ff')
            )
            g.append(
                draw.Lines(*Line(self.left_cut_p, self.cut_i).as_svg_coordinate_list, stroke="red")
            )
            g.append(
                draw.Lines(*Line(self.right_cut_p, self.cut_i).as_svg_coordinate_list, stroke="red")
            )
            g.append(
                draw.Lines(*self.left_seam_line.as_svg_coordinate_list, stroke="purple")
            )
            g.append(
                draw.Lines(*self.right_seam_line.as_svg_coordinate_list, stroke="purple")
            )
            return g

    style_debug = {
        "stroke": "grey",
        "style": "fill:none;stroke-width:0.3;stroke-dasharray:none"
    }
    style_corner_marker = {
        "stroke": "#333333",
        "style": "fill:none;stroke-width:1.8;stroke-dasharray:none;stroke-linecap:round;"
    }
    style_center = {
        "stroke": "red",
        "style": "fill:none;stroke-width:0.3;stroke-dasharray:none"
    }
    style_cut = {
        "stroke": "black",
        "style": "fill:none;stroke-width:0.4;stroke-dasharray:none;stroke-linecap:round;"
    }
    style_stitch_helper_line = {
        "stroke": "#666666",
        "style": "fill:none;stroke-width:0.3;stroke-dasharray:none;stroke-linecap:round;"
    }
    style_stitch_point = {
        "style": "fill:black;stroke-width:0.0;stroke-dasharray:none"
    }
    style_stitch_circle = {
        "stroke": "black",
        "style": "fill:none;stroke-width:0.3;stroke-dasharray:none"
    }

    def __init__(
        self,
        rest: Rest,
        side_lip_width: float = 6,
        bottom_lip_width: float = 8,
        bottom_seam_distance: float = 3.4,
        side_seam_distance: float = 3,
        corner_seam_stitch_distance: float = 3
    ):
        self.rest = rest
        self.top_layer_box_points = self._calc_top_layer_box_points
        self.top_layer_fillet_points = self._calc_top_layer_fillet_points
        self.top_fillet_corner_delta_y = math.fabs(self.top_layer_box_points[0].y - self.top_layer_fillet_points[0].y)
        self.top_fillet_corner_delta_x = Line(
            self.top_layer_fillet_points[2],
            Line(
                self.top_layer_fillet_points[2],
                self.top_layer_fillet_points[2]
                +
                (self.top_layer_fillet_points[2] - self.top_layer_fillet_points[1]).rotate(90)
            ).intersection_with(
                Line(
                    self.top_layer_box_points[1], self.top_layer_box_points[2]
                )
            )
        ).length

        self.wall_approx_path_len_side = self._calc_wall_approx_len(
            base_width=rest.width_back,
            middle_scale=rest.cushion_c.middle_layer_scale_x,
            top_scale=rest.cushion_c.top_layer_scale_x
        ) + self.top_fillet_corner_delta_x

        self.wall_approx_path_len_face = self._calc_wall_approx_len(
            base_width=rest.length,
            middle_scale=rest.cushion_c.middle_layer_scale_y,
            top_scale=rest.cushion_c.top_layer_scale_y
        ) + self.top_fillet_corner_delta_y

        self.overlength_fillet_correction = (
            self.rest.vertical_fillet
            -
            (2 * math.pi * self.rest.vertical_fillet * 0.125)
        )
        front_x_overlength = self.rest.width_front / 2 - self.top_layer_fillet_points[0].x - self.overlength_fillet_correction
        back_x_overlength = self.rest.width_back / 2 - self.top_layer_fillet_points[3].x - self.overlength_fillet_correction
        y_overlength = self.rest.length / 2 - self.top_layer_fillet_points[3].y - self.overlength_fillet_correction

        self.corners = [
            LeatherPattern.Corner(
                offset=p,
                angle_direction=d,
                bottom_overlength_left=over_left,
                bottom_overlength_right=over_right,
                side_length_left=wall_height_left,
                side_length_right=wall_height_right,
                lip_length=side_lip_width,
                corner_seam_stitch_distance=corner_seam_stitch_distance,
            )
            # br, bl, tl, tr
            for p, d, over_left, over_right, wall_height_left, wall_height_right in zip(
                self.top_layer_fillet_points,
                [225, 135, 45, 315],
                [y_overlength, front_x_overlength, y_overlength, back_x_overlength],
                [front_x_overlength, y_overlength, back_x_overlength, y_overlength],
                [
                    self.wall_approx_path_len_side,
                    self.wall_approx_path_len_face,
                    self.wall_approx_path_len_side,
                    self.wall_approx_path_len_face,
                ],
                [
                    self.wall_approx_path_len_face,
                    self.wall_approx_path_len_side,
                    self.wall_approx_path_len_face,
                    self.wall_approx_path_len_side,
                ],
            )
        ]

        self.lip_lines = self._factor_bottom_lines(offset=bottom_lip_width)
        self._shorten_lines(self.lip_lines, self.rest.vertical_fillet / 2)  # The reduction length is pretty
        self.bottom_seam_lines = self._factor_bottom_lines(offset=bottom_seam_distance)
        self.side_seam_lines = self._factor_bottom_lines(offset=-side_seam_distance)
        self.anchor_stitch_points = self._calc_anchor_stitch_points()

    @staticmethod
    def _shorten_lines(lines: list[Line], reduction_length: float):
        """
        Each line from a given list is shortened on both ends by the given value.
        """
        for line in lines:
            reduction_v = line.vector.unit_vector * reduction_length
            line.a = line.a + reduction_v
            line.b = line.b - reduction_v

    def _calc_anchor_stitch_points(self) -> list[Vector]:
        """
        Returns all stich points for the seam on the anchor in one big list.

        This method kind of re-implements the position calculation for the stich holes from the Rest class.
        It would be nice to have this re-factored. The implementation in the Rest class is anyway a bit
        awkward.
        """
        # front-back-back-front
        base_points = [Vector.from_tuple(t) for t in self.rest.get_base_points()]
        fillet_d = 2 * self.rest.vertical_fillet
        start_offset = self.rest.vertical_fillet - self.overlength_fillet_correction
        d = self.rest.anchor_c.stitch_distance
        stitch_length_front = Line(base_points[0], base_points[-1]).length - fillet_d
        stitch_length_back = Line(base_points[1], base_points[2]).length - fillet_d
        stitch_length_side = Line(base_points[0], base_points[1]).length - fillet_d

        result = []
        lengths = [stitch_length_side, stitch_length_front, stitch_length_side, stitch_length_back]

        for length, line in zip(lengths + lengths, self.bottom_seam_lines + self.side_seam_lines):
            num_x = math.floor(length / d) + 1
            side_offset = (length % d) / 2
            unit = line.vector.unit_vector
            delta_v = unit * d
            start_v = line.a + unit * (side_offset + start_offset)
            for i in range(num_x):
                result.append(start_v + delta_v * i)

        return result

    def _calc_wall_approx_len(
        self,
        base_width: float,
        middle_scale: float,
        top_scale: float
    ) -> float:
        return sum([
            line.length
            for line in VectorPath([
                Vector(0, 0),
                Vector(self.rest.cushion_c.straight_segment_height, 0),
                Vector(
                    self.rest.cushion_c.middle_layer_height,
                    (base_width - base_width * middle_scale) / 2
                ),
                Vector(
                    self.rest.cushion_c.height,
                    (base_width - base_width * top_scale) / 2
                ),
            ]).lines
        ]) + self.rest.anchor_c.base_height

    def _factor_bottom_lines(self, offset: float) -> list[Line]:
        return [
            Line(a, b).translate(
                (b - a).rotate(90).unit_vector * offset
            )
            for a, b in [
                (self.corners[-1].right_leg, self.corners[0].left_leg),
                (self.corners[0].right_leg, self.corners[1].left_leg),
                (self.corners[1].right_leg, self.corners[2].left_leg),
                (self.corners[2].right_leg, self.corners[3].left_leg),
            ]
        ]

    @property
    def _calc_top_layer_box_points(self) -> list[Vector]:
        indent = self.rest.cushion_c.upper_cutout_indent + self.rest.cushion_c.lower_cutout_indent
        x_front = self.rest.cushion_c.top_layer_scale_x * self.rest.width_front / 2
        x_back = self.rest.cushion_c.top_layer_scale_x * self.rest.width_back / 2
        y = self.rest.cushion_c.top_layer_scale_y * self.rest.length / 2
        indentation_extra_front = math.sqrt(x_front**2 + indent**2) - x_front
        indentation_extra_back = math.sqrt(x_back**2 + indent**2) - x_back
        x_front += indentation_extra_front
        x_back += indentation_extra_back
        return [
            Vector(x_front, -y),
            Vector(-x_front, -y),
            Vector(-x_back, y),
            Vector(x_back, y),
        ]

    @property
    def _calc_top_layer_fillet_points(self) -> list[Vector]:
        """
        front[+/-], back[+,-]
        """
        fillet_r = self.rest.vertical_fillet * self.rest.cushion_c.top_layer_fillet_scale
        offset_length = (math.sqrt(2 * (fillet_r ** 2)) - fillet_r)  # + self.rest.seam_indent
        alpha = self.rest.side_angle / 2
        bp = self.top_layer_box_points
        return [
            bp[0] + Vector(0, offset_length).rotate(+(45 - alpha)),
            bp[1] + Vector(0, offset_length).rotate(-(45 - alpha)),
            bp[2] - Vector(0, offset_length).rotate(+(45 + alpha)),
            bp[3] - Vector(0, offset_length).rotate(-(45 + alpha)),
        ]

    @property
    def basin_group(self) -> draw.Group:
        distance_to_middle_cutout = self.rest.foam_c.distance_to_middle_basin
        point_tuples = self.rest.get_base_points(
            x_reduction=distance_to_middle_cutout[0],
            y_reduction=distance_to_middle_cutout[1],
        )
        point_list = sum([[*list(p)] for p in point_tuples], [])
        g = draw.Group(**self.style_cut)
        g.append(
            draw.Lines(
                *point_list,
                close=True
            )
        )
        return g

    @property
    def bar_group(self) -> draw.Group:
        x = self.rest.anchor_c.bar_width
        y = self.rest.anchor_c.bar_length

        points1 = VectorPath()
        points1.append(Vector(0, 0))
        points1.append_relative(Vector(0, y))
        points1.append_relative(Vector(x, 0))
        points1.append_relative(Vector(0, -y))
        points1.append_relative(Vector(-x, 0))

        points2 = points1.translate(Vector(x + 2, 0))

        point_list1 = sum([[*v.as_tuple] for v in points1], [])
        point_list2 = sum([[*v.as_tuple] for v in points2], [])

        g = draw.Group(transform="translate({},{})".format(
            x, y/2
        ), **self.style_cut)
        g.append(
            draw.Lines(
                *point_list1,
                close=True
            )
        )
        g.append(
            draw.Lines(
                *point_list2,
                close=True
            )
        )
        return g

    @property
    def anchor_stitch_group(self) -> draw.Group:
        g = draw.Group()
        for stitch_p in self.anchor_stitch_points:
            g.extend([
                draw.Circle(*stitch_p.as_tuple, 0.8, **self.style_stitch_point),
                draw.Circle(*stitch_p.as_tuple, 1.5, **self.style_stitch_circle),
            ])
        return g

    @property
    def corner_stitch_group(self) -> draw.Group:
        g = draw.Group()
        for corner in self.corners:
            for stitch_p in corner.stitch_points:
                g.extend([
                    draw.Circle(*stitch_p.as_tuple, 0.4, **self.style_stitch_point),
                    draw.Circle(*stitch_p.as_tuple, 1, **self.style_stitch_circle),
                ])
        return g

    @property
    def leather_pattern_debug_group(self) -> draw.Group:
        g = draw.Group()
        g.append(
            draw.Lines(
                *sum([[*v.as_tuple] for v in self._calc_top_layer_box_points], []),
                close=True,
                **self.style_debug
            )
        )
        g.append(
            draw.Lines(
                *sum([[*v.as_tuple] for v in self.top_layer_fillet_points], []),
                close=True,
                **self.style_center
            )
        )
        for corner in self.corners:
            sub_g = draw.Group(**self.style_debug)
            sub_g.append(corner.debug_lines)
            g.append(sub_g)

        for line in self.lip_lines:
            g.append(
                draw.Lines(*line.as_svg_coordinate_list, stroke="orange", stroke_width="0.3")
            )

        for line in self.bottom_seam_lines:
            g.append(
                draw.Lines(*line.as_svg_coordinate_list, stroke="purple", stroke_width="0.3")
            )

        for line in self.side_seam_lines:
            g.append(
                draw.Lines(*line.as_svg_coordinate_list, stroke="purple", stroke_width="0.3")
            )

        g.append(self.anchor_stitch_group)
        g.append(self.corner_stitch_group)

        return g

    @property
    def leather_pattern_group(self) -> draw.Group:
        g = draw.Group()

        # Corner markers
        g.extend([
            draw.Line(*p1.as_tuple, *p2.as_tuple, **self.style_corner_marker)
            for p1, p2 in zip(self.top_layer_fillet_points, self.top_layer_box_points)
        ])

        last_corner = self.corners[-1]

        for corner, lip_line in zip(self.corners, self.lip_lines):
            g.append(
                draw.Lines(
                    *last_corner.right_leg.as_tuple,
                    *lip_line.as_svg_coordinate_list,
                    *corner.left_leg.as_tuple,
                    *corner.left_cut_p.as_tuple,
                    *corner.cut_i.as_tuple,
                    *corner.right_cut_p.as_tuple,
                    *corner.right_leg.as_tuple,
                    **self.style_cut
                )
            )
            last_corner = corner

        for line in self.bottom_seam_lines + self.side_seam_lines:
            g.append(
                draw.Lines(
                    *line.shorten(both=self.rest.vertical_fillet * 1/2).as_svg_coordinate_list,
                    **self.style_stitch_helper_line
                )
            )

        g.append(self.anchor_stitch_group)
        g.append(self.corner_stitch_group)

        return g

    @property
    def drawing(self) -> draw.Drawing:
        return self._drawing(debug=False)

    @property
    def debug_drawing(self) -> draw.Drawing:
        return self._drawing(debug=True)

    def _drawing(self, debug: bool) -> draw.Drawing:
        d = draw.Drawing(210, 297)
        d.width = "210mm"
        d.height = "297mm"
        d.viewBox = ("0", "0", "210", "297")

        g = draw.Group(transform="translate(105,200)")
        g.append(
            self.leather_pattern_debug_group
            if debug else
            self.leather_pattern_group
        )
        d.append(g)

        g = draw.Group(transform="translate(60, 60)")
        g.append(self.basin_group)
        d.append(g)

        g = draw.Group(transform="translate(130, 60)")
        g.append(self.bar_group)
        d.append(g)

        return d


cushion_c = CushionConfig()
anchor_c = AnchorConfig()
mold_c = MoldConfig()
foam_fill_c = FoamFillConfig()
base_c = BaseConfig(
    elevation_height=2.2,
)

rest = Rest(
    cushion_config=cushion_c,
    anchor_config=anchor_c,
    mold_config=mold_c,
    foam_fill_config=foam_fill_c,
    base_config=base_c,
)

pattern = LeatherPattern(
    rest,
    bottom_seam_distance=3.9,
)

#cushion_wp = rest.cushion.translate(cq.Vector(0, 0, rest.anchor_c.base_height))
# base_wp = rest.base.translate(cq.Vector(0, 0, -4))
#anchor_wp = rest.cusion_anchor.translate(cq.Vector(0, 0, -50))

#cusion_lower = rest.cushion_mold_lower_part.translate(cq.Vector(0, 0, 0))
#cusion_upper = rest.cushion_mold_upper_part.translate(cq.Vector(0, 0, 50))

squeeze = rest.leather_squeeze.translate(cq.Vector(0, 0, rest.anchor_c.base_height + 1))
#cushion_tool = rest.cushion_tool

#test = rest.cushion.translate(cq.Vector(0, 0, rest.anchor_c.base_height)) - rest.cushion_mold_lower_part

#pattern = rest.pattern
#show_object(pattern)

if __name__ == "__main__":
    output_format = "stl"
    output_dir = get_output_base_dir()
    tolerance = 0.001
    print(" Drawing \"leather-pattern.svg\"...")
    pattern.drawing.saveSvg(os.path.join(output_dir, "leather-pattern.svg"))
    print(" Drawing \"leather-pattern-debug.svg\"...")
    pattern.debug_drawing.saveSvg(os.path.join(output_dir, "leather-pattern-debug.svg"))
    #sys.exit(0)
    print(" Calculating 3D-model...")
    for part_name, part in [
        ("part-anchor", rest.cusion_anchor),
        ("part-base", rest.base),
        ("mold-upper", rest.cushion_mold_upper_part.rotate((0, 0, 0), (1, 0, 0), 180)),
        ("mold-lower", rest.cushion_mold_lower_part),
        ("tool-cushion-dummy", rest.cushion_tool),
        ("tool-leather-squeeze", rest.leather_squeeze.rotate((0, 0, 0), (1, 0, 0), 180)),
        ("test-cushion", rest.cushion),
        ("test-silicone-wrap", rest.cushion.translate(cq.Vector(0, 0, rest.anchor_c.base_height)) - rest.cushion_mold_lower_part),
    ]:
        print(" Rendering \"{}\"...".format(part_name))
        file_name = "{part}.{format}".format(
            part=part_name,
            format=output_format
        )
        out_file_path = os.path.join(output_dir, file_name)
        cq.exporters.export(part, out_file_path, tolerance=tolerance)
