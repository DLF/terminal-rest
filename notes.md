# Build Notes
* On `t10`, 2 × 38 ml silicone is a good amount.

# To Do's

## [Done] Clearance between anchor and cushion
Add clearance to the silicone part (effectively to the mold).

## [Done] Extra cut-out
For the front, add an extra conical cutout to the lower cut-out to give some more freedom to the wrist join.

Take out magic number and maybe group the cushion settings and the basic settings into different objects.
It's getting too much already.

## [Done] Cushion-Optimization
Try to maximize horizontal fillets and maybe increase height of the cushion.

## [Done] Seam channel
Add the seam channel to the four corners.

## [Done] Move out magic numbers for base-form fillets

## [Done] Chamfer on bottom of anchor plate

## [Done] Leather-Helper
Guess I will need the "cushion-negativ-tool" to properly strech and hold the leather when cutting and punching.

This must be in the cushion as well as in the anchor.

## [Done] Slightly increase screw hole diameter (by .4, I guess)

## [Done] Fix height of leather-squeeze base plate
The plate is ~5mm to hight. There must be some calculation mistake.

# t4 & t5

## [Done - I hope] Check if the bars are correctly placed
The t4 test print seems like the bars are a bit too much on the outside.
Like if the clearance is completely on the inside.

Just check it. Maybe it's just the print, but maybe there is a calcualtion issue.

## [Done] Nut-hole smaller
Nut has too much clearance.

## [Done] Lower as much as possible; foam also into the anchor
* [x] Align the anchor basin and the foam form so that the foam goes also into the anchor
* [x] Lower the bars as much as possible
* [x] Lower the cushion as much as possible

## [Done] Reconfigure
* [x] Slightly increase the outer base-form fillets
* [x] Upper cut-out sligtly wider
* [x] Upper cut-out less deep
* [x] Lower cutout sligthly wider
* [x] Front- and back-wall (silicone) much thinner

## [Done] Base
* Indent screw holes by at least 0.5mm

## [Done] Leather-Pattern
* [x] Increase wall-height by delta between top-corner and top-edge
* [x] Increase fillet-points abs(x-position) by middle indentation of leather
* [x] Correct bottom-corner points for fillet
* [x] Add bottom/side seam points
* [x] Add corner seam points

# t7 & t8
## [Done] Eliminate the basin; Increase the cushion height by the 2mm from the basin.
* [x] Check height of foam-bars!
* [x] Check outer wall curve; make it smooth again.
* [x] Think about making the whole rest 5mm wider (enlarge y)

# t10 & 11
## [Done] Cushion-Tool: Decrease bar indent to get a better print

## [Done] Rename build files
Have clear file name for `tool`, `mold`, `test`, `part`.

## [Done] Feasible base form for squeeze tool
